package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {
	
	@Autowired
	ProductRepository productRepository;

	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	public Product getProductById(int productId) {
		Product product = new Product(0, "Product Not Found!!!", 0.0);
		return productRepository.findById(productId).orElse(product);
	}

	public List<Product> getProductByName(String productName) {
		return productRepository.findByName(productName);
	}
	public Product addProduct(Product product) {
		return productRepository.save(product);
	}
	public Product updateProductById(int productId, Product updatedProduct) {
		Product existingProduct = productRepository.findById(productId).orElse(null);
		if (existingProduct != null) {
			existingProduct.setProdName(updatedProduct.getProdName());
            existingProduct.setPrice(updatedProduct.getPrice());
            return productRepository.save(existingProduct);
        } else {
        	return new Product(0, "Product Not Found!!!", 0.0);
		}
	}
	public boolean deleteProductById(int productId) {
		if (productRepository.existsById(productId)) {
			productRepository.deleteById(productId);
			return true;
		} else {
			return false;
		}
	}
}